import { RollPF } from "@dice/roll.mjs";

/**
 * Formula Field
 */
export class FormulaField extends foundry.data.fields.StringField {
  constructor(options = {}, context) {
    if (options.choices) throw new Error("choices is not valid option for FormulaField");
    super(options, context);
  }

  /**
   * @override
   * @param {object} config
   * @returns {HTMLInputElement}
   */
  _toInput(config) {
    /** @type {HTMLInputElement} */
    const input = super._toInput(config);

    input.classList.add("formula");

    const formula = config?.value;
    if (formula) {
      try {
        RollPF.parse(formula);
      } catch (err) {
        console.warn(err.message, { path: config.name, formula });
        input.classList.add("error");
      }
    }

    return input;
  }
}
