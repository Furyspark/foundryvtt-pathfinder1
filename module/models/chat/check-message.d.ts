export {};

declare module "./check-message.mjs" {
  interface CheckMessageModel extends BaseMessageModel {
    /**
     * Difficulty Class
     */
    dc?: object;
  }
}
