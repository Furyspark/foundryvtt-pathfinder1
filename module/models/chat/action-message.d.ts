export {};

declare module "./action-message.mjs" {
  interface ActionConfigData {
    /**
     * Critical Multiplier
     */
    critMult?: number;
  }

  /**
   * Action Data in chat message
   */
  interface ActionMessageData {
    /**
     * Action ID
     */
    id: string;
    /**
     * Action Name
     */
    name?: string;
    /**
     * Action Description
     */
    description?: string;
    /**
     * Save DC
     */
    dc?: number;
  }

  interface ActionMessageModel extends ItemMessageModel {
    config: ActionConfigData;
    rolls?: object;
    targets: Array<string>;
    action: ActionMessageData;
  }
}
