export {};

declare module "./damage-part-model.mjs" {
  interface DamagePartModel {
    /**
     * Damage formula
     */
    formula: string;
    /**
     * Damage type IDs
     *
     * Is an array in raw data instead.
     */
    types: Set<String>;
  }
}
