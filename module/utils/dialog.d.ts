export {};

declare module "./dialog.mjs" {
  interface getItemOptions extends ItemSelectorOptions {
    /**
     * Basic filter for item type, unused if filterFunc is provided.
     */
    type: string;
    /**
     * Basic filter for item subtype, unused if filterFunc is provided.
     */
    subType: string;
  }
}
