/**
 * Convert Distance
 *
 * @example
 * With metric enabled
 * ```hbs
 * {{convertDistance 5}} -> 1.5
 * ```
 * With imperial
 * ```hbs
 * {{convertDistance 9 "m"}} -> 30
 * ```
 *
 * @see {@link pf1.utils.convertDistance}
 * @internal
 * @group Handlebars
 * @param {number} value
 * @param {string} [type]
 */
export function convertDistance(value, type) {
  return Number.isFinite(value)
    ? pf1.utils.convertDistance(value, typeof type === "string" ? type : undefined)[0]
    : value;
}

/**
 * Action Range
 *
 * @example
 * ```hbs
 * {{actionRange someAction actionRollData}} -> 30 ft
 * ```
 *
 * @group Handlebars
 * @internal
 * @param {pf1.components.ItemAction} action
 * @param {object} rollData
 * @returns {string}
 */
export function actionRange(action, rollData) {
  if (!action?.hasRange) return null;

  const range = action.range.value;
  const rangeType = action.range.units;

  if (!rangeType) return null;
  if (rangeType === "spec") return null; // Special is its own thing

  const [rng, unit] = pf1.utils.calculateRange(range, rangeType, rollData);
  return `${rng} ${unit}`;
}

/**
 * Action Damage
 *
 * @example
 * ```hbs
 * {{actionDamage someAction}} -> 3d6
 * ```
 * @see {@link pf1.utils.formula.actionDamage}
 * @group Handlebars
 * @internal
 * @param {ItemAction} action - Action
 * @param {object} [_rollData] - Deprecated
 * @param {object} [options] - Additional options
 * @returns {string} - Damage string
 */
export function actionDamage(action, _rollData, options) {
  if (!action.hasDamage) return null;
  return pf1.utils.formula.actionDamage(action, { simplify: options?.hash?.combine ?? true });
}

/**
 * Alt numberFormat helper to provide non-fixed point decimals and pretty fractionals
 *
 * @example
 * ```hbs
 * {{numberFormatAlt 5.52 decimals=1}} -> 5.5
 * {{numberFormatAlt 5 decimals=1}} -> 5
 * {{numberFormatAlt 5.5 fraction=true}} -> 5 1/2
 * ```
 * Compare with core helper
 * ```hbs
 * {{numberFormat 5.52 decimals=1}} -> 5.5
 * {{numberFormat 5 decimals=1}} -> 5.0
 * ```
 *
 * @group Handlebars
 * @internal
 * @param {number} number
 * @param {object} [context]
 * @param {object} [context.hash]
 * @param {boolean} [context.hash.fraction] - Format as fractional.
 * @param {number} [context.hash.decimals] - Display up to this many decimals. Has no effect with fractional.
 * @returns {number|string} - Formatted number
 */
export function numberFormatAlt(number, { hash } = {}) {
  if (hash.fraction) return pf1.utils.fractionalToString(number);
  else return pf1.utils.limitPrecision(number, hash.decimals);
}

/**
 * Register Handlebars Helpers as part of system initialization
 *
 * @group Handlebars
 * @internal
 */
export function register() {
  const helpers = {
    convertDistance,
    actionRange,
    actionDamage,
    numberFormatAlt,
  };

  for (const [id, fn] of Object.entries(helpers)) {
    Handlebars.registerHelper(id, fn);
  }
}
