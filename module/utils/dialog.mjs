import { ItemSelector } from "module/applications/item-selector.mjs";
import { ActorSelector } from "module/applications/actor-selector.mjs";

/**
 * Choose actor from list.
 *
 * This is simplified interface to {@link pf1.applications.ActorSelector}
 *
 * @param {ActorSelectorOptions} options - Selection options
 * @returns {Promise<string|null>} - Actor ID or null if cancelled.
 */
export async function getActor(options) {
  return ActorSelector.wait(options);
}

/**
 * Choose item from actor.
 *
 * This is simplified interface to {@link pf1.applications.ItemSelector}
 *
 * @param {getItemOptions} options - Options
 * @returns {Promise<Item|null>} - Chosen item or null.
 */
export async function getItem(options) {
  if (!options.type && !options.subType && !options.filterFunc && !options.items)
    throw new Error("Insufficient filter rules provided.");

  options.filterFunc ||= (item) => {
    return (!options.type || item.type === options.type) && (!options.subType || item.subType === options.subType);
  };

  if (!options.window) options.window = {};

  // Provide nice basic title
  if (!options.window.title && !options.filterFunc && options.type) {
    options.window.title = game.i18n.format("PF1.SelectSpecific", {
      specifier: options.subType
        ? pf1.config[`${options.type}Types`]?.[options.subType]
        : game.i18n.localize(CONFIG.Item.typeLabels[options.type]),
    });
  }

  return ItemSelector.wait(options);
}

/**
 * Query for a number from current user.
 *
 * @example
 * ```js
 * const num = await pf1.utils.dialog.getNumber({
 *   placeholder: "NaN",
 *   hint: "Amazing",
 *   label: "Gimme a number",
 * });
 * ```
 *
 * @param {object} [options={}] Additional options
 * @param {number} [options.min=null] Minimum value
 * @param {number} [options.max=null] Maximum value
 * @param {number} [options.step] Value stepping
 * @param {string} [options.title] Dialog title
 * @param {string} [options.label] A label preceding the number input.
 * @param {string} [options.hint] A hint displayed under the input.
 * @param {number} [options.initial] Initial value
 * @param {string} [options.placeholder] Placeholder value or description.
 * @param {string[]} [options.classes=[]] CSS classes to add.
 * @param {Function} [options.render] Render callback. Passed onto DialogV2.
 * @param {object} [options.dialog={}] Additional dialog options. Merged with the other DialogV2 options.
 * @returns {Promise<number|null>} Provided value or null if dialog was cancelled.
 */
export async function getNumber({
  title,
  label,
  hint,
  initial,
  placeholder,
  min,
  max,
  step,
  classes = [],
  render: renderCallback,
  dialog: dialogOptions = {},
} = {}) {
  const templateData = { value: initial, min, max, step, placeholder, label, hint };
  const content = await renderTemplate("systems/pf1/templates/apps/get-number.hbs", templateData);

  const options = {
    window: { title: title || game.i18n.localize("PF1.Application.GetNumber.Title") },
    classes: ["pf1-v2", "get-number", ...classes],
    content,
    position: {
      width: 260,
    },
    buttons: [
      {
        icon: "fa-solid fa-check",
        label: "PF1.Application.GetNumber.Confirm",
        action: "confirm",
        default: true,
        callback: (event, target, html) => html.querySelector("form input[name='number']").valueAsNumber,
      },
    ],
    render: (event, html) => {
      // Select contents of the number input
      html.querySelector("form input[name='number']").select();

      renderCallback?.(event, html);
    },
    close: () => NaN,
    rejectClose: false,
  };

  return foundry.applications.api.DialogV2.wait(foundry.utils.mergeObject(options, dialogOptions));
}
