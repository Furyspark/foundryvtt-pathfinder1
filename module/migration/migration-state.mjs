import { MigrationCategory } from "./migration-category.mjs";

/**
 * State object for tracking migration progress.
 */
export class MigrationState {
  /**
   * Display label
   *
   * @type {string}
   */
  label;

  /**
   * @type {Record<number, Function>}
   */
  callbacks = {};

  /**
   * @type {Record<string, MigrationCategory>}
   */
  categories = {};

  completed = false;

  /**
   * Compendium unlocking state
   *
   * @type {boolean}
   */
  unlock = false;

  constructor(label) {
    if (label) label = game.i18n.localize(label);
    this.label = label;
  }

  /**
   * @param {string} category - Category ID
   * @param {string} label - Display label
   * @param {boolean} isNumber - Does the category track a number
   * @returns {MigrationCategory} - Created category
   */
  createCategory(category, label, isNumber) {
    if (!this.categories[category]) {
      this.categories[category] = new MigrationCategory(category, label, isNumber, this);
      this.emit(this.categories[category], { action: "new" });
    }
    return this.categories[category];
  }

  /**
   * @param {MigrationCategory|MigrationState} category - Category or the overall state
   * @param {object} info - Category or state specific data
   */
  emit(category, info) {
    for (const callback of Object.values(this.callbacks)) {
      try {
        callback(this, category, info);
      } catch (err) {
        console.error(err, callback);
      }
    }
  }

  start() {
    this.completed = false;
    this.emit(this, { action: "start" });
  }

  finish() {
    this.completed = true;
    this.emit(this, { action: "finish" });
  }

  get errors() {
    return Object.values(this.categories).reduce((total, c) => total + c.errors.length, 0);
  }

  get invalid() {
    return Object.values(this.categories).reduce((total, c) => total + c.invalid, 0);
  }

  get ignored() {
    return Object.values(this.categories).reduce((total, c) => total + c.ignored, 0);
  }
}
