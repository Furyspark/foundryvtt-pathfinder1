import { ItemPF } from "./item-pf.mjs";

/**
 * Feature item
 *
 * Class features, feats, traits, templates, racial traits, etc.
 */
export class ItemFeatPF extends ItemPF {
  /** @inheritDoc */
  static system = Object.freeze({ ...super.system, subtypeName: true });

  /**
   * @internal
   * @override
   * @param {object} context - Context
   * @param {User} userId - Triggering user ID
   */
  _onDelete(context, userId) {
    super._onDelete(context, userId);

    if (game.users.get(userId)?.isSelf) {
      if (this.isActive) {
        this.executeScriptCalls("toggle", { state: false });
      }
    }
  }

  /** @inheritDoc */
  _activeStateChange(changed) {
    const active = changed.system?.disabled;
    if (active !== undefined) return !active;
  }

  /** @inheritDoc */
  async setActive(active, context) {
    return this.update({ "system.disabled": !active }, context);
  }

  /** @inheritDoc */
  get isActive() {
    return this.system.disabled !== true;
  }

  /** @inheritDoc */
  getLabels({ actionId, rollData } = {}) {
    const labels = super.getLabels({ actionId, rollData });
    const { subType, abilityType } = this.system;

    labels.subType = pf1.config.featTypes[subType];
    labels.featType = pf1.config.featTypes[subType];

    labels.abilityType = pf1.config.abilityTypes[this.system.abilityType]?.short;
    if (this.subType === "trait") {
      labels.traitType = pf1.config.traitTypes[this.system.traitType];
    } else if (this.subType === "racial") {
      labels.raceType = pf1.config.raceTypes[this.system.traitType];
      labels.traitCategory = pf1.config.racialTraitCategories[this.system.traitCategory];
    }

    // Ability type
    if (abilityType && abilityType !== "none") {
      labels.abilityType = pf1.config.abilityTypes[abilityType].short;
      labels.abilityTypeLong = pf1.config.abilityTypes[abilityType].long;
    } else if (labels.abilityType) {
      delete labels.abilityType;
    }

    return labels;
  }

  /** @inheritDoc */
  _addTypeRollData(result) {
    // Add fake CL based on associated class level or HD
    result.cl = result.class?.level || result.attributes?.hd?.total;
  }

  /** @inheritDoc */
  getTypeChatData(data, labels, props, rollData) {
    super.getTypeChatData(data, labels, props, rollData);
    // Add ability type label
    if (labels.abilityType) {
      props.push(labels.abilityType);
    }
  }
}
