_id: 055IbpMbY3yfbwPb
_key: '!items!055IbpMbY3yfbwPb'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Exoskeleton
system:
  changes:
    - _id: o4t3xp26
      formula: '2'
      target: str
      type: untyped
  crOffset: '0'
  description:
    value: >-
      <p><strong>Acquired/Inherited Template</strong> Acquired<br
      /><strong>Simple Template</strong> No<br /><strong>Usable with
      Summons</strong> No</p><p>Found skittering through forgotten tombs,
      crawling through deep forests, and filling damp caverns, exoskeletons are
      animated carapaces of arthropods and other vermin. Most exoskeletons are
      the intentional creations of necromancers, but some of these undead
      monstrosities arise spontaneously from places awash with negative energy
      or are created by malfunctioning artifacts. Sometimes, the simple act of
      feeding on freshly destroyed undead creatures is enough to transform an
      insect into an exoskeleton upon its eventual death. Though exoskeletons
      are just as mindless as they were when they were living, they have become
      infused with an evil instinct in their new unlife that drives them to
      relentlessly attack all living creatures on sight, exploding in a burst of
      dusty remains when destroyed.</p><p>A spellcaster can create an
      exoskeleton using
      @UUID[Compendium.pf1.spells.Item.8uwmrygxgih1fb57]{animate dead}. An
      exoskeleton can be created from a mostly intact dead vermin that has an
      exoskeleton. This includes arachnids, crustaceans, insects, and even some
      mollusks, but not soft-bodied vermin such as jellyfish and
      leeches.</p><p>"Exoskeleton" is an acquired template that can be added to
      any corporeal vermin that has an exoskeleton (referred to hereafter as the
      base creature).</p><p><strong>Challenge Rating:</strong> Depends on Hit
      Dice, as follows.</p><hr /><table><tbody><tr><td><p><strong>Hit
      Dice</strong></p></td><td><p><strong>CR</strong></p></td><td><p><strong>XP</strong></p></td></tr><tr><td><p>1</p></td><td><p>1/4</p></td><td><p>100</p></td></tr><tr><td><p>2</p></td><td><p>1/2</p></td><td><p>200</p></td></tr><tr><td><p>3-4</p></td><td><p>1</p></td><td><p>400</p></td></tr><tr><td><p>5-6</p></td><td><p>2</p></td><td><p>600</p></td></tr><tr><td><p>7-8</p></td><td><p>3</p></td><td><p>800</p></td></tr><tr><td><p>9-10</p></td><td><p>4</p></td><td><p>1,200</p></td></tr><tr><td><p>11-12</p></td><td><p>5</p></td><td><p>1,600</p></td></tr><tr><td><p>13-15</p></td><td><p>6</p></td><td><p>2,400</p></td></tr><tr><td><p>16-17</p></td><td><p>7</p></td><td><p>3,200</p></td></tr><tr><td><p>18-20</p></td><td><p>8</p></td><td><p>4,800</p></td></tr><tr><td><p>21-24</p></td><td><p>9</p></td><td><p>6,400</p></td></tr><tr><td><p>25-28</p></td><td><p>10</p></td><td><p>9,600</p></td></tr></tbody></table><hr
      /><p><br /><strong>Alignment:</strong> Always neutral
      evil.</p><p><strong>Type:</strong> The creature’s type changes to undead.
      It retains any subtype except for alignment subtypes and subtypes that
      indicate kind. It does not gain the augmented subtype. It uses the base
      creature’s abilities except as noted below.</p><p><strong>Armor
      Class:</strong> The base creature’s natural armor changes as
      follows.</p><hr /><table><tbody><tr><td><p><strong>Exoskeleton
      Size</strong></p></td><td><p><strong>Natural Armor
      Bonus</strong></p></td></tr><tr><td><p>Tiny or
      smaller</p></td><td><p>+0</p></td></tr><tr><td><p>Small</p></td><td><p>+1</p></td></tr><tr><td><p>Medium</p></td><td><p>+2</p></td></tr><tr><td><p>Large</p></td><td><p>+3</p></td></tr><tr><td><p>Huge</p></td><td><p>+4</p></td></tr><tr><td><p>Gargantuan</p></td><td><p>+7</p></td></tr><tr><td><p>Colossal</p></td><td><p>+11</p></td></tr></tbody></table><hr
      /><p><br /><strong>Hit Dice:</strong> An exoskeleton retains the number of
      Hit Dice the base creature had, and gains a number of additional Hit Dice
      as noted on the following table. If the base creature has more than 20 Hit
      Dice, it can’t be made into an exoskeleton by the <em>animate dead</em>
      spell. An exoskeleton uses its Charisma modifier (instead of its
      Constitution modifier) to determine bonus hit points.</p><hr
      /><table><tbody><tr><td><p><strong>Exoskelton
      Size</strong></p></td><td><p><strong>Bonus Hit
      Dice</strong></p></td></tr><tr><td><p>Tiny or
      smaller</p></td><td><p>—</p></td></tr><tr><td><p>Small or
      Medium</p></td><td><p>+1</p></td></tr><tr><td><p>Large</p></td><td><p>+2</p></td></tr><tr><td><p>Huge</p></td><td><p>+4</p></td></tr><tr><td><p>Gargantuan</p></td><td><p>+6</p></td></tr><tr><td><p>Colossal</p></td><td><p>+10</p></td></tr></tbody></table><hr
      /><p><br /><strong>Saves:</strong> Base save bonuses are Fort +1/3 Hit
      Dice, Ref +1/3 Hit Dice, and Will +1/2 Hit Dice +
      2.</p><p><strong>Defensive Abilities:</strong> Exoskeletons lose their
      defensive abilities and gain all of the qualities and immunities granted
      by the undead type. In addition, exoskeletons gain DR
      5/bludgeoning.</p><p><strong>Speed:</strong> Exoskeletons retain all
      movement speeds. They can still fly but their maneuverability drops to
      clumsy.</p><p><strong>Attacks:</strong> An exoskeleton retains all of its
      natural weapons. If the base creature didn’t have any natural weapons, it
      gains a slam attack that deals damage as if it were one size category
      larger than its actual size.</p><p><strong>Special Attacks:</strong> An
      exoskeleton loses all of its special attacks that rely on a living biology
      (such as poison), but it retains any
      others.</p><p><strong>Abilities:</strong> An exoskeleton’s Strength
      increases by 2. The exoskeleton has no Constitution or Intelligence score,
      and its Wisdom and Charisma scores change to
      10.</p><p><strong>BAB:</strong> An exoskeleton’s base attack bonus is
      equal to 3/4 of its Hit Dice.</p><p><strong>Skills:</strong> Though most
      vermin are mindless and have no skill ranks, the exoskeleton loses all
      skill ranks if it had any, and it doesn’t retain any racial bonuses it
      had.</p><p><strong>Feats:</strong> An exoskeleton loses all feats that the
      base creature had and doesn’t gain feats as its Hit Dice increase, but it
      does gain @UUID[Compendium.pf1.feats.Item.8snLqsJN4LLL00Nq]{Toughness} as
      a bonus feat.</p><p><strong>Special Qualities:</strong> An exoskeleton
      loses most special qualities of the base creature. It retains any
      extraordinary special qualities that improve its melee or ranged attacks.
      An exoskeleton gains the following special quality.</p><p><em>Burst (Ex):
      </em>When an exoskeleton is destroyed, its desiccated husk bursts,
      releasing the dusty remains of the vermin’s insides into the surrounding
      air. Any creature adjacent to an exoskeleton when it bursts must succeed
      at a Fortitude save or become staggered for 1 round as it coughs and
      sneezes. Creatures that don’t need to breathe are immune to this effect.
      If the exoskeleton has 10 or more Hit Dice, the victim is instead
      nauseated for 1 round. The save DC is equal to 10 + half the exoskeleton’s
      Hit Dice + its Charisma modifier.</p>
  subType: template
type: feat
