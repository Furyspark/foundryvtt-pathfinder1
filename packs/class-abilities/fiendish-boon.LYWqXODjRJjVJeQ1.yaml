_id: LYWqXODjRJjVJeQ1
_key: '!items!LYWqXODjRJjVJeQ1'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/weapon_29.jpg
name: Fiendish Boon
system:
  abilityType: sp
  associations:
    classes:
      - Antipaladin
  description:
    value: >-
      <p>Upon reaching 5th level, an antipaladin receives a boon from his dark
      patrons. This boon can take one of two forms. Once the form is chosen, it
      cannot be changed.</p><p>The first type of bond allows the antipaladin to
      enhance his weapon as a standard action by calling upon the aid of a
      fiendish spirit for 1 minute per antipaladin level. When called, the
      spirit causes the weapon to shed unholy light as a torch. At 5th level,
      this spirit grants the weapon a +1 enhancement bonus. For every three
      levels beyond 5th, the weapon gains another +1 enhancement bonus, to a
      maximum of +6 at 20th level. These bonuses can be added to the weapon,
      stacking with existing weapon bonuses to a maximum of +5, or they can be
      used to add any of the following weapon properties: <em>anarchic</em>,
      <em>flaming</em>, <em>flaming burst</em>, <em>keen</em>, <em>speed</em>,
      <em>unholy</em>, <em>vicious</em>, <em>vorpal</em>, and <em>wounding</em>.
      Adding these properties consumes an amount of bonus equal to the
      property's cost. These bonuses are added to any properties the weapon
      already has, but duplicate abilities do not stack. If the weapon is not
      magical, at least a +1 enhancement bonus must be added before any other
      properties can be added. The bonus and properties granted by the spirit
      are determined when the spirit is called and cannot be changed until the
      spirit is called again. The fiendish spirit imparts no bonuses if the
      weapon is held by anyone other than the antipaladin but resumes giving
      bonuses if returned to the antipaladin. These bonuses apply to only one
      end of a double weapon. An antipaladin can use this ability once per day
      at 5th level, and one additional time per day for every four levels beyond
      5th, to a total of four times per day at 17th level.</p><p>If a weapon
      bonded with a fiendish spirit is destroyed, the antipaladin loses the use
      of this ability for 30 days, or until he gains a level, whichever comes
      first. During this 30-day period, the antipaladin takes a –1 penalty on
      attack and weapon damage rolls.</p><p>The second type of bond allows an
      antipaladin to gain the service of a fiendish servant. This functions as
      @Compendium[pf1.spells.1zmn8vqy3s5l9kiv]{Summon Monster III}, except the
      duration is permanent and the antipaladin can only gain the service of a
      single creature and that creature must either have the chaotic and evil
      subtypes or it must be a fiendish animal. Once selected, the choice is
      set, but it may be changed whenever the antipaladin gains a level. Upon
      reaching 7th level, and every two levels thereafter, the level of the
      <em>summon monster</em> spell increases by one, to a maximum of
      @Compendium[pf1.spells.mi52crelhwuy6bmp]{Summon Monster IX} at 17th
      level.</p><p>Once per day, as a full-round action, an antipaladin may
      magically call his servant to his side. This ability is the equivalent of
      a spell of a level equal to one-third the antipaladin's level. The servant
      immediately appears adjacent to the antipaladin. An antipaladin can use
      this ability once per day at 5th level, and one additional time per day
      for every four levels thereafter, for a total of four times per day at
      17th level.</p><p>At 11th level, the servant gains the advanced template
      (see the Pathfinder RPG Bestiary). At 15th level, an antipaladin's servant
      gains spell resistance equal to the antipaladin's level + 11.</p><p>Should
      the antipaladin's fiendish servant die or be banished, the antipaladin may
      not summon another servant for 30 days or until he gains an antipaladin
      level, whichever comes first. During this 30-day period, the antipaladin
      takes a –1 penalty on attack and weapon damage rolls.</p>
  subType: classFeat
type: feat
