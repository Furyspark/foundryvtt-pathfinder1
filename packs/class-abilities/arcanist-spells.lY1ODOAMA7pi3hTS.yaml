_id: lY1ODOAMA7pi3hTS
_key: '!items!lY1ODOAMA7pi3hTS'
_stats:
  coreVersion: '12.331'
img: icons/magic/light/hand-sparks-smoke-teal.webp
name: Arcanist Spells
system:
  associations:
    classes:
      - Arcanist
  description:
    value: >-
      <p>An arcanist casts arcane spells drawn from the sorcerer/wizard spell
      list. An arcanist must prepare her spells ahead of time, but unlike a
      wizard, her spells are not expended when they’re cast. Instead, she can
      cast any spell that she has prepared consuming a spell slot of the
      appropriate level, assuming she hasn’t yet used up her spell slots per day
      for that level.</p><p>To learn, prepare, or cast a spell, the arcanist
      must have an Intelligence score equal to at least 10 + the spell’s level.
      The saving throw DC against an arcanist’s spell is 10 + the spell’s level
      + the arcanist’s Intelligence modifier.</p><p>An arcanist can only cast a
      certain number of spells of each spell level per day. Her base daily spell
      allotment is given on
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.8HNFlPoMR5NxGMzD#class-features]{Table:
      Arcanist}. In addition, she receives bonus spells per day if she has a
      high Intelligence score (see
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.BfKeMBHQruwqyUXX#table-1-3:-ability-modifiers-and-bonus-spells]{Table:
      Ability Modifiers and Bonus Spells}).</p><p>An arcanist may know any
      number of spells, but the number she can prepare each day is limited. At
      1st level, she can prepare four 0-level spells and two 1st-level spells
      each day. At each new arcanist level, the number of spells she can prepare
      each day increases, adding new spell levels as indicated on
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.8HNFlPoMR5NxGMzD#spells-prepared]{Table:
      Spells Prepared}. Unlike the number of spells she can cast per day, the
      number of spells an arcanist can prepare each day is not affected by her
      Intelligence score. Feats and other effects that modify the number of
      spells known by a spellcaster instead affect the number of spells an
      arcanist can prepare.</p><p>An arcanist must choose and prepare her spells
      ahead of time by getting 8 hours of sleep and spending 1 hour studying her
      spellbook. While studying, the arcanist decides what spells to prepare and
      refreshes her available spell slots for the day.</p><p>Like a sorcerer, an
      arcanist can choose to apply any metamagic feats she knows to a prepared
      spell as she casts it, with the same increase in casting time (see
      Spontaneous Casting and Metamagic Feats on page 113 of the Core Rulebook).
      However, she may also prepare a spell with any metamagic feats she knows
      and cast it without increasing casting time like a wizard. She cannot
      combine these options—a spell prepared with metamagic feats cannot be
      further modified with another metamagic feat at the time of casting
      (unless she has the
      @UUID[Compendium.pf1.class-abilities.Item.kUjXL9ec9TGCoJYq]{metamixing}
      arcanist exploit, detailed below).</p>
  sources:
    - id: PZO1115
      pages: '121'
  subType: classFeat
type: feat
