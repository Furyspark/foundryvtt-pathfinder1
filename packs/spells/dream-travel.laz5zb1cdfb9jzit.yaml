_id: laz5zb1cdfb9jzit
_key: '!items!laz5zb1cdfb9jzit'
_stats:
  coreVersion: '12.331'
folder: Ix6Dc5gXEobiIHKD
img: systems/pf1/icons/misc/magic-swirl.png
name: Dream Travel
system:
  actions:
    - _id: 2b07tktftggy141d
      actionType: spellsave
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      duration:
        dismiss: true
        units: hour
        value: '@cl'
      name: Use
      range:
        units: touch
      save:
        description: Will negates
        type: will
      target:
        value: you and one creature/level
  components:
    somatic: true
    verbal: true
  description:
    value: >-
      <p>You and the other targets of the spell are physically drawn from the
      Material Plane into the Dimension of Dreams on a voyage into the dreams of
      a creature you designate. In the Dimension of Dreams, you move through a
      swirling sea of thoughts, desires, and emotions created by the minds of
      dreamers everywhere to reach your destination dreamscape. Reaching the
      destination dreamscape takes 1 hour. At any point before the spell's
      duration ends, you can dismiss the spell to return to where you started on
      the Material Plane. The connection between dreams and reality is
      inherently tenuous, and your ability to arrive precisely where you mean to
      is dependent on your familiarity with the dreamer you're trying to find.
      To determine how accurate your arrival is at the end of your <i>dream
      travel</i>, roll d% on the following table. </p><table
      border="1"><tbody><tr><th>Familiarity</th><th>On Target</th><th>Off
      Target</th><th>Other</th><th>Dreamscape Mishap</th></tr><tr><td>Very
      familiar</td><td>1-97</td><td>98-99</td><td>100</td><td>-</td></tr><tr><td>Somewhat
      familiar</td><td>1-94</td><td>95-97</td><td>98-99</td><td>100</td></tr><tr><td>Known
      creature</td><td>1-88</td><td>89-94</td><td>95-98</td><td>99-100</td></tr><tr><td>Not
      well
      known</td><td>1-76</td><td>77-88</td><td>89-96</td><td>97-100</td></tr><tr><td>False
      identity</td><td>-</td><td>-</td><td>81-92</td><td>93-100</td></tr></tbody></table>
      <br /><b>Familiarity</b>: "Very familiar" means that you have had contact
      using <i>dream</i>, <i>dream council</i>, <i>dream scan</i>, or a similar
      spell within the past 24 hours with the creature whose dreamscape you are
      trying to locate. "Somewhat familiar" means that you have had contact with
      the dreaming character using one of those spells at least once in the
      past. "Known creature" means that while you know the creature whose
      dreamscape you're trying to locate, you have not connected to its dreams
      with those spells. "Not well known" is a creature you have heard of, know
      by name and true identity, but have never met.  "False identity" means
      that whether or not you have met the creature, you know it only through a
      false identity. When trying to locate the dreamscape of a creature known
      to you through a false identity, roll 1d20 + 80 on the table to obtain
      your results (instead of rolling d%), since there is no way for you to
      pinpoint the correct dreamscape. <br /><b>On Target</b>: You travel to the
      correct creature's current dreamscape. <br /><b>Off Target</b>: You travel
      to an area near the target dreamscape on the Ethereal Plane. <br
      /><b>Other Dreamscape</b>: You travel to a similar creature's dreamscape
      on the Dimension of Dreams. <br /><b>Mishap</b>: You and anyone else
      traveling with you experience a mishap during travel; each character takes
      1d10 points of damage and must reroll on the table to see where it ends
      up. For these rerolls, roll 1d20 + 80. Each time "Mishap" comes up, the
      travelers take more damage and must reroll to see where they end up. 
      Regardless of the accuracy of your <i>dream travel</i>, you and your
      companions all arrive at the same location (except in the case of a
      mishap). Mindless creatures can't use <i>dream travel</i>, nor can
      creatures that can't dream.  You might be able to exit the dream near the
      creature on its home plane. If you and the other creatures are still in a
      dream when the dreamer wakes up, the dreamer can decide to bring you out
      onto its plane within 1d10 miles of itself. If it chooses not to, you and
      the other travelers are pushed into another dreamscape or onto the
      Ethereal Plane. The spell ends when the creature wakes up, so you no
      longer have the option to dismiss the spell and return home. However, you
      do get enough warning that a spell is about to end that you can dismiss it
      before the dreamer awakens and decides whether to allow you to arrive at
      its location.  You can use <i>dream travel</i> to travel to the dream of a
      creature on another plane, but this requires trekking through the dreams
      of outsiders. It takes 1d4+1 hours of uninterrupted travel to reach the
      destination dream, and each traveler must succeed at a Will save each hour
      spent traveling in this fashion. The DC of this save is 10 for the first
      hour, and increases by 5 for each hour thereafter. If a creature fails
      this saving throw, it becomes shaken for the remainder of the <i>dream
      travel</i> and for a number of hours thereafter equal to the amount of
      time spent using <i>dream travel</i> to reach the destination plane. After
      the first time a creature fails the saving throw, one of the following
      effects occurs (determined randomly by the GM). These effects are
      considered emotion effects, in addition to any other descriptors that
      apply. <table
      border="1"><tbody><tr><th>d6</th><th>Result</th></tr><tr><td>1</td><td>Creature
      contracts cackle fever or mindfire (equal chance of either; see page 557
      of the Pathfinder RPG Core Rulebook).</td></tr><tr><td>2</td><td>Creature
      is cursed, as bestow curse; if the creature is a spellcaster, it instead
      acquires a random minor spellblight (Pathfinder RPG Ultimate Magic
      94).</td></tr><tr><td>3</td><td>Creature is attacked by a phantasmal
      killer, and is treated as having failed the initial saving throw to
      disbelieve it. The Fortitude save DC is equal to the Will save DC that the
      creature failed.</td></tr><tr><td>4</td><td>Creature is possessed as
      possession (see page 181) by an outsider (50% chance to be a creature from
      the destination plane; otherwise, it is a random hostile
      outsider).</td></tr><tr><td>5</td><td>Creature is affected as
      feeblemind.</td></tr><tr><td>6</td><td>Creature is affected as
      insanity.</td></tr></tbody></table><p>Traveling to a different planar
      destination decreases the accuracy of your dream transit. If you exit a
      creature's dream onto a plane different from the plane you started on, you
      end up in a random location on the destination plane.  If the target of
      your <i>dream travel</i> isn't dreaming, you and the other travelers can
      wait on the Dimension of Dreams until that creature begins sleeping. For
      each hour that passes during this wait, each <i>dream travel</i>er must
      attempt a Will save as if it were traveling to reach another plane.</p>
  descriptors:
    - mindAffecting
  learnedAt:
    class:
      mesmerist: 6
      psychic: 6
  level: 6
  school: con
  sources:
    - id: PZO1132
      pages: '166'
  subschool:
    - teleportation
type: spell
