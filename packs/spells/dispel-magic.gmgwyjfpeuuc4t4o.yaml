_id: gmgwyjfpeuuc4t4o
_key: '!items!gmgwyjfpeuuc4t4o'
_stats:
  coreVersion: '12.331'
folder: 4NHp2XrrZ02oHYog
img: icons/magic/symbols/runes-star-orange-purple.webp
name: Dispel Magic
system:
  actions:
    - _id: lqs5d6wvxet6j1fw
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      area: one spellcaster, creature, or object
      duration:
        units: inst
      name: Use
      range:
        units: medium
      target:
        value: one spellcaster, creature, or object
  components:
    somatic: true
    verbal: true
  description:
    value: >-
      <p>You can use <em>dispel magic</em> to end one ongoing spell that has
      been cast on a creature or object, to temporarily suppress the magical
      abilities of a magic item, or to counter another spellcaster's spell. A
      dispelled spell ends as if its duration had expired. Some spells, as
      detailed in their descriptions, can't be defeated by <em>dispel
      magic</em>. <em>Dispel magic</em> can dispel (but not counter) spell-like
      effects just as it does spells. The of a spell with an instantaneous
      duration can't be dispelled, because the magical effect is already over
      before the <em>dispel magic</em> can take effect.</p><p>You choose to use
      <em>dispel magic</em> in one of two ways: a targeted dispel or a
      counterspell.</p><p><em>Targeted Dispel</em>: One object, creature, or
      spell is the target of the <em>dispel magic</em> spell. You make one
      dispel check (1d20 + your caster level) and compare that to the spell with
      highest caster level (DC = 11 + the spell's caster level). If successful,
      that spell ends.</p><p>If not, compare the same result to the spell with
      the next highest caster level. Repeat this process until you have
      dispelled one spell affecting the target, or you have failed to dispel
      every spell.</p><p>For example, a 7th-level caster casts <em>dispel
      magic</em>, targeting a creature affected by <em>stoneskin</em> (caster
      level 12th) and <em>fly</em> (caster level 6th). The caster level check
      results in a 19. This check is not high enough to end the
      <em>stoneskin</em> (which would have required a 23 or higher), but it is
      high enough to end the <em>fly</em> (which only required a 17). Had the
      dispel check resulted in a 23 or higher, the <em>stoneskin</em> would have
      been dispelled, leaving the <em>fly</em> intact. Had the dispel check been
      a 16 or less, no spells would have been affected.</p><p>You can also use a
      targeted dispel to specifically end one spell affecting the target or one
      spell affecting an area (such as a <em>wall of fire</em>). You must name
      the specific spell effect to be targeted in this way. If your caster level
      check is equal to or higher than the DC of that spell, it ends. No other
      spells or effects on the target are dispelled if your check is not high
      enough to end the targeted effect.</p><p>If you target an object or
      creature that is the effect of an ongoing spell (such as a monster
      <em>summoned</em> by <em>summon monster</em>), you make a dispel check to
      end the spell that conjured the object or creature.</p><p>If the object
      that you target is a magic item, you make a dispel check against the
      item's caster level (DC = 11 + the item's caster level). If you succeed,
      all the item's magical properties are suppressed for 1d4 rounds, after
      which the item recovers its magical properties. A suppressed item becomes
      nonmagical for the duration of the effect. An interdimensional opening
      (such as a <em>bag of holding</em>) is temporarily closed. A magic item's
      physical properties are unchanged: A suppressed magic sword is still a
      sword (a masterwork sword, in fact). Artifacts and deities are unaffected
      by mortal magic such as this.</p><p>You automatically succeed on your
      dispel check against any spell that you cast
      yourself.</p><p><em>Counterspell</em>: When <em>dispel magic</em> is used
      in this way, the spell targets a spellcaster and is cast as a
      counterspell. Unlike a true counterspell, however, <em>dispel magic</em>
      may not work; you must make a dispel check to counter the other
      spellcaster's spell.</p>
  learnedAt:
    bloodline:
      Arcane: 3
    class:
      antipaladin: 3
      arcanist: 3
      bard: 3
      cleric: 3
      druid: 4
      hunter: 4
      inquisitor: 3
      magus: 3
      medium: 3
      mesmerist: 3
      occultist: 3
      oracle: 3
      paladin: 3
      psychic: 3
      shaman: 3
      skald: 3
      sorcerer: 3
      spiritualist: 3
      summoner: 3
      summonerUnchained: 3
      warpriest: 3
      witch: 3
      wizard: 3
    domain:
      Magic: 3
    subDomain:
      Entropy: 3
  level: 3
  school: abj
  sources:
    - id: PZO1110
      pages: '272'
  sr: false
type: spell
